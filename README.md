# Search fields gadget for Atlassian JIRA

Are your users searching in specific set of issues? Are they looking only for bugs in certain state. assets in inventory, invoices?

Provide them with the simple gadget on shared dashboard with up to three fields.

Conditions entered into fields will be combined with predefined filter, either as FilterJQL AND (A OR B OR C), or FilterJQL AND (A AND B AND C). 
User will bre redirected to the single issue or the issue navigator.

The configuration is rather rudimentary: operators are predefined to LIKE (if available) or EQUAL. 

## Usage

### Configuration

1. Add gadget to your dashboard
2. Configure with one of shared filters
3. Configure fields to use with filter and *Save*.

### Search
1. Select proper gadget.
2. Fill in values and *Submit*. Values are used as-is, no validating/parsing is done.
3. Single issue is shown, or issue navigator with columns configured for predefined filter.

## Development

- use standard Atlassian SDK
- testing database is from 7.x H2
- for running integration tests use `-Dtestgroups=jira` for JIRA 7.x

### Test database

- contains predefined filters Demo (only for jira-users), Tasks (shared for everyone)
- configured system dasbboard and admin dashboard

## Issues/Enhancements

See issue tracker at https://bitbucket.org/eeasro/search-fields-gadget/issues

