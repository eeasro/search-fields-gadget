package it;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.gadgets.GadgetContainer;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.TestedProductFactory;
import it.com.atlassian.gadgets.pages.Gadget;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class BaseTest {
    // look for XXX-renderbox
    private static String GADGET_ID="gadget-10200";

    @Test
    public void isGadgetVisible() throws IOException {
        JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);
        DashboardPage dashBoardPage = jira.gotoLoginPage().loginAsSysAdmin(DashboardPage.class);
        GadgetContainer gadgets = dashBoardPage.gadgets();
        Gadget gadget = gadgets.get(GADGET_ID);
        assertNotNull(gadget);
        System.out.println("gadget title: "+gadget.getTitleBar().getText());
    }
}
