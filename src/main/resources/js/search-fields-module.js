define("sk.eea.jira.search-fields-dashboard-item", [
    'underscore',
    'jquery',
    'jira-dashboard-items/components/filter-picker'
], function (
    _,
    $,
    FilterPicker
) {
    var DashboardItem = function (API) {
        this.API = API;
    };
    var decodeNonASCII = function (text) {
        return $('<div>').html(text).html();
    };
    /**
     * Called to render the view for a fully configured dashboard item.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item (e.g. filter id, number of results...)
     */
    DashboardItem.prototype.render = function (context, preferences) {
        var self = this;
        var displayPrefs = $.extend({}, preferences);
        var $element = this.$element = $(context);
        self.API.showLoadingBar();

        //Init result view of the gadget
        context.empty().html(Plugin.Item.Filter.view());
        var $form = this.$form = $element.find("#searchform");

        if (displayPrefs.searchtitle === "" || displayPrefs.searchtitle === undefined) {
            // Get filter name by id
            $.ajax({
                type: "GET",
                url: AJS.contextPath() + "/rest/eea-search-fields/1.0/gadget/getfiltername",
                data: {
                   id: displayPrefs.searchfilter
                },
                success: _.bind(function (data) {
                   self.API.setTitle(AJS.I18n.getText(
                       "sk.eea.jira.gadget.search.fields.title.with.filter", data));
                }, this),
                error: _.bind(function (data) {
                    context.prepend(Plugin.Item.Filter.error());
                    self.API.resize();
                }, this)
            });
        } else {
            self.API.setTitle(AJS.I18n.getText(
                "sk.eea.jira.gadget.search.fields.title.with.filter", decodeNonASCII(displayPrefs.searchtitle)));
        }

        $.ajax({
            type: "GET",
            url: AJS.contextPath() + "/rest/eea-search-fields/1.0/gadget/searchfieldnames",
            data: {
               id1: displayPrefs.searchfield1,
               id2: displayPrefs.searchfield2,
               id3: displayPrefs.searchfield3
            },
            success: _.bind(function(data) {
               //Init textfields for search field queries
               for(var i = 2; i >= 0; i--) {
                   if (data[i]) {
                       $form.prepend(Plugin.Item.Filter.textField({
                           label: data[i],
                           required: false,
                           id: "text-field-" + i
                       }))
                   }
               }

               self.API.hideLoadingBar();
               self.API.resize();
            }, this),
            error: _.bind(function(data) {
                self.API.hideLoadingBar();
                context.empty().html(Plugin.Item.Filter.error());
                self.API.resize();
            }, this)
        });


        //Event handlers
        $form.submit(_.bind(function (e) {
            e.preventDefault();
            self.API.showLoadingBar();

            $.ajax({
                       type: "GET",
                       url: AJS.contextPath()
                            + "/rest/eea-search-fields/1.0/gadget/diredirect",
                       data: {
                           filterid: displayPrefs.searchfilter,
                           mode: displayPrefs.searchmode,
                           value1: context.find("#text-field-0").val(),
                           value2: context.find("#text-field-1").val(),
                           value3: context.find("#text-field-2").val(),
                           id1: displayPrefs.searchfield1,
                           id2: displayPrefs.searchfield2,
                           id3: displayPrefs.searchfield3
                       },
                       success: _.bind(function (data) {
                           window.location.replace(data.URI);
                       }, this),
                       error: _.bind(function (data) {
                           self.API.hideLoadingBar();
                           context.empty().html(Plugin.Item.Filter.error());
                           self.API.resize();
                       }, this)
                   });
        }, this));
    };
    /**
     * Called to render the configuration form for this dashboard item if preferences.isConfigured
     * has not been set yet.
     *
     * @param context The surrounding <div/> context that this items should render into.
     * @param preferences The user preferences saved for this dashboard item
     */
    DashboardItem.prototype.renderEdit = function (context, preferences) {
        var self = this;
        var prefix = self.API.getGadgetId() + "-";
        self.API.showLoadingBar();

        //Init config view of the gadget
        context.empty().html(Plugin.Item.Filter.Config(
            {
                preferences: {
                    isConfigured: preferences.isConfigured
                },
                modeOptions: [
                    {
                        label: AJS.I18n.getText(
                            'sk.eea.jira.gadget.search.fields.mode.or'),
                        value: 'OR'
                    },
                    {
                        label: AJS.I18n.getText(
                            'sk.eea.jira.gadget.search.fields.mode.and'), value: 'AND'
                    }
                ],
                selectedValues: {
                    mode: preferences.searchmode,
                    name: preferences.searchtitle === undefined ? "" : _.escape(decodeNonASCII(preferences.searchtitle)),
                    first: preferences.searchfield1,
                    second: preferences.searchfield2,
                    third: preferences.searchfield3
                }
            }));

        var form = context.find("form");

        /**
         * Retrieves new value -> first matching from configuration without operator.
         *
         * @param data option list as retrieved from REST endpoint ../serchfields, where all option ids are ended with operator
         * @param legacy potentially old form without operator
         */
        function convertOldFieldId(data, legacy) {
            var op = legacy.slice(-1);
            if (op === '=' || op === '~') return legacy;
            for (var i=0; i < data.length; i++) {
                if (data[i].value.slice(0,-1) === legacy) return data[i].value;
            }
            return legacy;
        }

        $.ajax({
            type: "GET",
            url: AJS.contextPath() + "/rest/eea-search-fields/1.0/gadget/searchfields",
            success: _.bind(function(data) {
                console.log(data);
                //Init required field picker
                var params = {
                    label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.field.label"),
                    options: [{label:AJS.I18n.getText("sk.eea.jira.gadget.search.fields.empty.label"), value:""}].concat(data),
                    description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.field.description"),
                    id: 'search-field-1',
                    selected: convertOldFieldId(data, preferences.searchfield1),
                    required: true};
                form.find("fieldset:last").append(Plugin.Item.Filter.select($.extend(params)));
                $("#search-field-1").auiSelect2();
                //Init selects with optional searchable fields
                var paramsOptional = {
                    label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.optfield.label"),
                    options: [{label:AJS.I18n.getText("sk.eea.jira.gadget.search.fields.empty.label"), value:"-1"}].concat(data),
                    description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.optfield.description")
                };
                for(var i = 2; i <= 3; i++) {
                    var id = 'search-field-' + i;
                    form.find("fieldset:last").append(Plugin.Item.Filter.select(
                        $.extend(paramsOptional,{id: id, selected: convertOldFieldId(data, preferences['searchfield'+i])})
                    ));
                    $("#" + id).auiSelect2();
                }
                $("#mode-field").auiSelect2();
                self.API.hideLoadingBar();
                self.API.resize();
            }, this),
            error: _.bind(function(data) {
                self.API.hideLoadingBar();
                context.empty().html(Plugin.Item.Filter.error());
                self.API.resize();
            }, this)
        });

        //Init the filter picker
        form.find("fieldset:first")
            .prepend(JIRA.DashboardItem.Common.Config.Templates.filterPicker(
                {
                    prefix: prefix,
                    id: "saved-filter"
                }));
        //For compatibility with old gadget impl.
        //preferences.searchfilter default value is "-1"
        //Filter picker considers "-1" a valid value, making erroneous ajax call
        preferences.searchfilter = preferences.searchfilter === "-1"
            ? undefined : preferences.searchfilter;

        self.filterPicker = new FilterPicker().init(
            {
                errorContainer: context.find(".dashboard-item-error"),
                element: form.find("#" + prefix + "saved-filter"),
                selectedValue: preferences.searchfilter !== undefined
                    ? preferences.searchfilter.replace('filter-', '') : preferences.searchfilter,
                parentElement: form
            });
        self.API.resize();

        //Event handlers
        form.on("submit", _.bind(function (e) {
            e.preventDefault();
            var form = $(e.target);

            onSave({
                       searchtitle: form.find("#searchtitle").val(),
                       searchmode: form.find("#mode-field").val(),
                       searchfield1: form.find("#search-field-1").val(),
                       searchfield2: form.find("#search-field-2").val(),
                       searchfield3: form.find("#search-field-3").val()
                   });
        }, this));

        form.find("input.aui-button.cancel").on("click", _.bind(function () {
            this.API.closeEdit();
        }, this));

        function onSave(prefs) {
            var validFilter = self.filterPicker.validate();
            if (validFilter) {
                var selectedFilter = self.filterPicker.getValue();
                prefs.searchfilter = selectedFilter.id;
                self.API.savePreferences(prefs);
            } else {
                self.API.resize();
            }
        }
    };
    return DashboardItem;
});
