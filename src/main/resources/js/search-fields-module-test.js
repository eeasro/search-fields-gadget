/**
 * Created by mme on 7/20/16.
 */
AJS.test.require([
    "com.atlassian.jira.gadgets:common-test-resources",
    "sk.eea.jira.search-fields-gadget:search-fields-dashboard-item-resources",
    "com.atlassian.jira.gadgets:filter-results-dashboard-item-resources"
], function() {
    'use strict';

    var $ = require('jquery');

    module('sk.eea.jira.search-fields-dashboard-item', {
        fieldsUrlRegex: /\/jira\/rest\/eea-search-fields\/1\.0\/gadget\/searchfields\?_=\d+/,

        setup: function() {
            this.mockedContext = AJS.test.mockableModuleContext();

            this.API = $.extend({}, DashboardItem.Mocks.API);
            this.APIStub = sinon.stub(this.API);

            this.$el = $("<div/>");
            $("qunit-fixture").append(this.$el);
            this.server = sinon.fakeServer.create();
            this.server.respondImmediately = true;
        },

        teardown: function() {
            this.server.restore();
            this.$el.empty();
        },

        submitConfigFormWithValues: function(isFilterPickerValid) {
            this.mockedContext.mock('jira-dashboard-items/components/filter-picker', createMockPicker({id: "12345", label:"Tasks"}, isFilterPickerValid));


            var SearchFields = this.mockedContext.require('sk.eea.jira.search-fields-dashboard-item');
            var dashboardItem = new SearchFields(this.API);
            dashboardItem.renderEdit(this.$el, {});

            mockServerResponse(
                this.server,
                this.fieldsUrlRegex,
                [
                   200,
                   { "Content-Type": "application/json" },
                   JSON.stringify([{ label: "Project", value: 12000}])
            ]);

            this.$el.find("#searchtitle").val("aa");
            this.$el.find("#mode-field").val("AND");
            this.$el.find("#search-field-1").val(12000);
            this.$el.find("#search-field-2").val(12000);
            this.$el.find("#search-field-3").val(12000);
            this.$el.find("form").submit();
        },

        renderConfigForm: function(urlRegex, response) {
            this.mockedContext.mock('jira-dashboard-items/components/filter-picker', createMockPicker({id: "12345", label:"Tasks"}, true));

            var SearchFields = this.mockedContext.require('sk.eea.jira.search-fields-dashboard-item');
            var dashboardItem = new SearchFields(this.API);
            dashboardItem.renderEdit(this.$el, {});

            mockServerResponse(
                this.server,
                urlRegex,
                response
            );
        },

        renderViewForm: function(httpStatus, url) {
            this.mockedContext.mock('jira-dashboard-items/components/filter-picker', createMockPicker({id: "12345", label:"Tasks"}, true));

            var SearchFields = this.mockedContext.require('sk.eea.jira.search-fields-dashboard-item');
            var dashboardItem = new SearchFields(this.API);
            dashboardItem.render(this.$el, {});

            mockServerResponse(
                this.server,
                url,
                [
                    httpStatus,
                    { "Content-Type": "application/json" },
                    JSON.stringify(["Project =", "Summary ~", "Issue Type ="])
            ]);
        }
    });

    function createMockPicker(value, isValid) {
        return function () {
            return {
                init: function () {
                    return this;
                },
                getValue: function () {
                    return value;
                },
                validate: function () {
                    return isValid;
                }
            }
        }
    }

    function mockServerResponse(server, urlRegex, response) {
        server.respondWith(
            urlRegex,
            response
        );

        server.respond();
    }

    test("Validation of filter picker triggers error", function () {
        this.submitConfigFormWithValues(false);

        sinon.assert.called(this.APIStub.resize, "Layout refreshed after render");
        sinon.assert.notCalled(this.APIStub.savePreferences, "savePrefs wasn't called");
    });

    test("Save Prefs called once validation passes", function () {
        this.submitConfigFormWithValues(true);

        sinon.assert.calledOnce(this.APIStub.savePreferences, "savePrefs was called");
        sinon.assert.calledWith(this.APIStub.savePreferences, {
            searchfilter: "12345",
            searchtitle: "aa",
            searchmode: "AND",
            searchfield1: "12000",
            searchfield2: "12000",
            searchfield3: "12000"
        });
    });

    test("Searchable fields edit AJAX error", function(){
        this.renderConfigForm(
            this.fieldsUrlRegex,
            [
                404,
                { "Content-Type": "application/json" },
                JSON.stringify([{ error: "error"}])
        ]);

        ok(this.$el.has(".aui-message-error"));
    });

    test("Result has correct fields", function () {
        this.renderViewForm(200);

        var labelElements = this.$el.find("label");
        equal(labelElements.length, 3);
        equal(labelElements[0].textContent, "Project =");
        equal(labelElements[1].textContent, "Summary ~");
        equal(labelElements[2].textContent, "Issue Type =");
    });

    test("Searchable fields view AJAX error", function () {
        this.renderViewForm(404, /\/jira\/rest\/eea-search-fields\/1\.0\/gadget\/searchfieldnames\?.*/);

        ok(this.$el.has(".aui-message-error"));
    });

    test("Searchable fields view getting title AJAX error", function () {
        this.renderViewForm(404, /\/jira\/rest\/eea-search-fields\/1\.0\/gadget\/getfiltername\?.*/);

        ok(this.$el.has(".aui-message-error"));
        ok(this.$el.has(".button"));
    });
});