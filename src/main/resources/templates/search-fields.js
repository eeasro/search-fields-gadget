/**
 * Configuration of gadget
 */
function search_fields_gadget_configuration(baseUrl) {
    var optionsEmpty = [{label:AJS.I18n.getText("sk.eea.jira.gadget.search.fields.empty.label"), value:-1}];
    var gadget = AJS.Gadget({
        baseUrl: baseUrl,
        config: {
            descriptor: function (args) {
                return  {
                    theme : function () {
                        if (gadgets.window.getViewportDimensions().width < 450) { return "gdt top-label"; }
                        else { return "gdt";}
                    }(),
                    fields: [
                        {
                            userpref: "searchfilter",
                            label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.filter.label"),
                            description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.filter.description"),
                            type: "select",
                            selected: gadget.getPref("searchfilter"),
                            options: args.searchfilters
                        },
                        {
                            userpref: "searchtitle",
                            label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.title.label"),
                            description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.title.description"),
                            type: "text",
                            value: gadget.getPref("searchtitle")
                        },
                        {
                            userpref: "searchmode",
                            label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.mode.label"),
                            description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.mode.description"),
                            type: "select",
                            selected: gadget.getPref("searchmode"),
                            options: [
                                {label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.mode.or"), value: "OR"},
                                {label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.mode.and"), value: "AND"}]
                        },
                        {
                            userpref: "searchfield1",
                            label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.field.label"),
                            description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.field.description"),
                            type: "select",
                            selected: gadget.getPref("searchfield1"),
                            options: args.searchfields
                        },
                        {
                            userpref: "searchfield2",
                            label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.optfield.label"),
                            description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.optfield.description"),
                            type: "select",
                            selected: gadget.getPref("searchfield2"),
                            options: optionsEmpty.concat(args.searchfields)
                        },
                        {
                            userpref: "searchfield3",
                            label: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.optfield.label"),
                            description: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.optfield.description"),
                            type: "select",
                            selected: gadget.getPref("searchfield3"),
                            options: optionsEmpty.concat(args.searchfields)
                        },
                        AJS.gadget.fields.nowConfigured()
                    ]
                };
            },
            args: [{
                key: "searchfields",
                ajaxOptions: function() {
                    return {
                        url: "/rest/eea-search-fields/1.0/gadget/searchfields"
                    };
                }
            },{
                key: "searchfilters",
                ajaxOptions: function() {
                    return {
                        url: "/rest/eea-search-fields/1.0/gadget/searchfilters"
                    };
                }
            }]
        },
        view: {
            onResizeAdjustHeight: true,
            template: function(args) {
                var gadget = this;
                var hasFields = false;
                var form = AJS.$("<form/>").attr({
                    action: baseUrl+"/rest/eea-search-fields/1.0/gadget/redirect",
                    method: "GET",
                    target: "_parent"
                });
                form.addClass("aui gdt");

                var mode = gadget.getPref("searchmode");
                var title = gadget.getPref("searchtitle");
                var filterid = gadget.getPref("searchfilter");
                var filtername = args.getfiltername;
                if (filtername) {
                    form.append(AJS.$("<input/>").attr({type:"hidden", name:"filterid", value:filterid}));
                } else {
                    form.append(AJS.$("<div/>").addClass("aui-message error").text(AJS.I18n.getText("sk.eea.jira.gadget.search.fields.filter.missing")));
                    title = filterid + "?";
                }
                gadgets.window.setTitle(
                        AJS.format(AJS.I18n.getText("sk.eea.jira.gadget.search.fields.title.with.filter"), title ? title : filtername));

                form.append(AJS.$("<input/>").attr({type:"hidden",name:"mode", value: mode}));

                for (var x = 1; x <= 3; x++){
                    var id = gadget.getPref("searchfield" + x);
                    if(id != null && id != "" && id != -1) {
                        hasFields = true;

                        var divFields = AJS.$("<div/>").addClass("field-group");
                        form.append(divFields);
                        divFields.append(
                            AJS.$("<label/>").attr("for", "value"+x).text(args.searchfieldnames[x-1]));
                        divFields.append(
                            AJS.$("<input/>").attr({type:"hidden", name:"id" + x, value:gadget.getPref("searchfield" + x)}));
                        divFields.append(
                            AJS.$("<input/>").attr({type:"text", id:"value"+x, name:"value" + x}).addClass("text"));
                    }
                }

                if(!hasFields){
                    form.append(AJS.$("<div/>").addClass("aui-message error").text(AJS.I18n.getText("sk.eea.jira.gadget.search.fields.field.missing")));
                }
                else{
                    var divButtons = AJS.$("<div/>").addClass("field-group");
                    form.append(divButtons);
                    divButtons.append(
                        AJS.$("<input/>").addClass("button").attr({
                            type:"submit",
                            value: AJS.I18n.getText("sk.eea.jira.gadget.search.fields.submit.label")
                        }));
                }
                gadget.getView().html(form);
            },
            args: [{
                key: "searchfieldnames",
                ajaxOptions: function() {
                    return {
                        url: "/rest/eea-search-fields/1.0/gadget/searchfieldnames?id1=" + this.getPref("searchfield1") + "&id2=" + this.getPref("searchfield2") + "&id3=" + this.getPref("searchfield3")
                    };
                }
            },{
                key: "getfiltername",
                ajaxOptions: function() {
                    return {
                        url: "/rest/eea-search-fields/1.0/gadget/getfiltername?id=" + this.getPref("searchfilter")
                    };
                }
            }]
        }
    });
}
