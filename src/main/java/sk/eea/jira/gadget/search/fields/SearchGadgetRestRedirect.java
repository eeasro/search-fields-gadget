/*
 * Some developer decisions: https://answers.atlassian.com/questions/68271/gadget-user-preference-select-from-dynamic-list-what-is-the-best-way
 * */

package sk.eea.jira.gadget.search.fields;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.SearchableField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchHandler;
import com.atlassian.jira.issue.search.SearchHandler.ClauseRegistration;
import com.atlassian.jira.issue.search.SearchHandler.SearcherRegistration;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.builder.ConditionBuilder;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONEscaper;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;
import com.atlassian.query.operator.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Path("/gadget")
public class SearchGadgetRestRedirect {
    private static final Logger LOG = LoggerFactory.getLogger(SearchGadgetRestRedirect.class);

    private final SearchService searchService;
    private final JiraAuthenticationContext authenticationContext;
    private final FieldManager fieldManager;
    private final SearchRequestService searchRequestService;

    private final ApplicationProperties applicationProperties;
    private final static int SIZE = 3;

    public SearchGadgetRestRedirect(
            SearchService searchService,
            JiraAuthenticationContext authenticationContext,
            FieldManager fieldManager,
            SearchRequestService searchRequestService,
            ApplicationProperties applicationProperties) {
        this.searchService = searchService;
        this.authenticationContext = authenticationContext;
        this.fieldManager = fieldManager;
        this.searchRequestService = searchRequestService;
        this.applicationProperties = applicationProperties;
    }

    // @precondition: string.trim().equals(string)
    private boolean isParamEmpty(String string) {
        // http://marxsoftware.blogspot.sk/2011/09/checking-for-null-or-empty-or-white.html
        return string == null || string.isEmpty();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/searchfields")
    // @returns available text search fields
    public Response getIssueFieldList() {
        Set<SearchableField> fields = fieldManager.getAllSearchableFields();
        JSONArray array = new JSONArray();
        ClauseInformation information;
        for (SearchableField sf : fields) {
            if ((information = getFieldInformation(sf)) != null) {
                for (Operator operator : getOperators(information)) {
                    HashMap<String, String> object = new HashMap<>();
                    object.put("label", sf.getName() + " " + operatorToCharacter(operator));
                    //IMPORTANT! stores value as {ID+operator}
                    object.put("value", sf.getId() + operatorToCharacter(operator));
                    array.put(object);
                }
            }
        }
        return Response.ok(array.toString()).build();
    }

    private List<Operator> getOperators(ClauseInformation information) {
        Set<Operator> operators = information.getSupportedOperators();
        List<Operator> ops = new ArrayList<>();

        if (operators.contains(Operator.LIKE)) ops.add(Operator.LIKE);
        if (operators.contains(Operator.EQUALS)) ops.add(Operator.EQUALS);

        return ops;
    }

    /**
     * Returns clause information for constructing query with LIKE or EQUALS.
     * @param field system or custom field
     * @return null if no suitable handler is found
     */
    private ClauseInformation getFieldInformation(SearchableField field) {
        SearchHandler handler;
        if ((handler = field.createAssociatedSearchHandler()) == null) return null;

        SearcherRegistration registration;
        if ((registration = handler.getSearcherRegistration()) == null) return null;

        List<ClauseRegistration> clauseHandlers;
        if ((clauseHandlers = registration.getClauseHandlers()) == null) return null;

        for (ClauseRegistration cr : clauseHandlers) {
            ClauseHandler clauseHandler;
            if ((clauseHandler = cr.getHandler()) == null) continue;

            Set<Operator> operators = clauseHandler.getInformation().getSupportedOperators();
            if (operators.contains(Operator.LIKE) || operators.contains(Operator.EQUALS)) {
                return clauseHandler.getInformation();
            }
        }
        return null;
    }

    /**
     * Returns first suitable operator
     */
    private Operator guessFieldOperator(ClauseInformation clauseInformation) {
        if (clauseInformation != null) {
            Set<Operator> operators = clauseInformation.getSupportedOperators();
            if (operators.contains(Operator.LIKE)) {
                return Operator.LIKE;
            }
            if (operators.contains(Operator.EQUALS)) {
                return Operator.EQUALS;
            }
        }
        return null;
    }

    /**
     * Returns first suitable operator
     */
    private Operator guessFieldOperator(SearchableField field) {
        return guessFieldOperator(getFieldInformation(field));
    }

    private String operatorToCharacter(Operator operator) {
        return (operator != null) ? operator.getDisplayString() : "?";
    }

    /**
     * Gets available filters for the logged in user (the selected filter can be not visible for the user after time
     * so we implement a re-choose (isFilterAvailable))
     *
     * @return all available filters
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/searchfilters")
    public Response getAvailableFilters() {
        Collection<SearchRequest> filters = searchRequestService.getFavouriteFilters(authenticationContext.getLoggedInUser());
        JSONArray array = new JSONArray();
        for (SearchRequest sr : filters) {
            HashMap<String, String> object = new HashMap<>();
            object.put("label", sr.getName());
            object.put("value", sr.getId().toString());
            array.put(object);
        }
        return Response.ok(array.toString()).build();
    }

    /**
     * Checks whether active filter is available.
     *
     * @return filter name if is available, empty string otherwise
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getfiltername")
    public Response getFilterName(@QueryParam("id") final String id) {
        try {
            ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
            JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(loggedInUser, new SimpleErrorCollection());
            //id => "filter-10330"
            SearchRequest searchRequest = searchRequestService.getFilter(serviceContext, parseFilterId(id));
            if (searchRequest != null) {
                return Response.ok(String.format("%s%s\"", '"', JSONEscaper.escape(searchRequest.getName()))).build();
            }
        } catch (NumberFormatException nfe) {
            // pass
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    private SearchableField getFieldForId(String id) {
        Field field = (id != null && !id.equals("-1"))
                ? (containsOperator(id)) ? fieldManager.getField(isolateId(id)) : fieldManager.getField(id)
                : null;
        if (field instanceof SearchableField) return (SearchableField) field;
        else return null;
    }

    private SearchableField[] getFieldArrayForIds(String id1, String id2, String id3) {
        SearchableField[] sfArray = new SearchableField[3];
        sfArray[0] = getFieldForId(id1);
        sfArray[1] = getFieldForId(id2);
        sfArray[2] = getFieldForId(id3);
        return sfArray;
    }

    /**
     * Provides field names for selected fields.
     *
     * Fields are provided in format customfield_nnnnn[+~]? where operators where added in version 1.8.0.
     *
     * @return field name with used operations
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/searchfieldnames")
    public Response getSearchFieldNames(@QueryParam("id1") final String id1,
                                        @QueryParam("id2") final String id2,
                                        @QueryParam("id3") final String id3) {
        String[] idInputs = {id1, id2, id3};
        SearchableField[] fields = getFieldArrayForIds(id1, id2, id3);
        JSONArray array = new JSONArray();
        for (int i = 0; i < fields.length; i++) {
            array.put((fields[i] == null)
                    ? null
                    : (containsOperator(idInputs[i]))
                        ? fields[i].getName() + ' ' + isolateOperator(idInputs[i])
                        : fields[i].getName() + ' ' + operatorToCharacter(guessFieldOperator(fields[i])));
        }
        return Response.ok(array.toString()).build();
    }

    // name - name of the custom field (from gadget config); value - value of
    // the custom field (from the gadget form)
    @GET
    @AnonymousAllowed
    @Produces({MediaType.TEXT_HTML})
    @Path("/redirect")
    public Response redirect(@QueryParam("id1") final String id1,
                             @QueryParam("id2") final String id2,
                             @QueryParam("id3") final String id3,
                             @QueryParam("value1") final String value1,
                             @QueryParam("value2") final String value2,
                             @QueryParam("value3") final String value3,
                             @QueryParam("mode") final String mode,
                             @QueryParam("filterid") final String filterid) {

        UriBuilder builder;
        try {
            builder = generateIssuesUri(id1, id2, id3, value1, value2, value3, filterid, mode);
        } catch (SearchException e) {
            LOG.error("Search error while generating response to HTML request.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.temporaryRedirect(builder.build()).build();
    }

    /**
     * Returns a link to a search result according to the search params.
     * name - name of the custom field (from gadget config);
     * value - value of the custom field (from the gadget form)
     * @param filterid id of chosen filter
     * @param mode logical operator to be used when adding filter results with fields value
     * @param value1 value of the first field
     * @param value2 value of the second field
     * @param value3 value of the third field
     * @param id1 id of first field
     * @param id2 id of second field
     * @param id3 id of third field
     * @return link to search result
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/diredirect")
    public Response redirectDashboardItem(
            @QueryParam("id1") final String id1,
            @QueryParam("id2") final String id2,
            @QueryParam("id3") final String id3,
            @QueryParam("value1") final String value1,
            @QueryParam("value2") final String value2,
            @QueryParam("value3") final String value3,
            @QueryParam("mode") final String mode,
            @QueryParam("filterid") final String filterid)
    {
        UriBuilder builder;
        try {
            builder = generateIssuesUri(id1, id2, id3, value1, value2, value3, filterid, mode);
        } catch (SearchException e) {
            LOG.error("Search error while generating response to HTML request.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }


        String jsonString = null;
        try {
            jsonString = new JSONObject().put("URI", builder.build().toString()).toString();
        } catch (JSONException e) {
            LOG.error("Error creating JSON object",e);
        }
        return Response.ok(jsonString).build();
    }


    private Long parseFilterId(String filterId) {
        if (filterId != null) return Long.valueOf(filterId.startsWith("filter-") ? filterId.substring(7) : filterId);
        else return null;
    }

    private String parseValue(final String value) {
        return (value != null) ? value.trim() : "";
    }

    private UriBuilder generateIssuesUri(String id1,
                                         String id2,
                                         String id3,
                                         String value1,
                                         String value2,
                                         String value3,
                                         String filterid,
                                         String mode) throws SearchException {
        String[] idInputs = {id1, id2, id3};
        SearchableField[] fields = getFieldArrayForIds(id1, id2, id3);

        String[] params = new String[SIZE];
        params[0] = parseValue(value1);
        params[1] = parseValue(value2);
        params[2] = parseValue(value3);

        // suffixOr[i] = true <=> !isParamEmpty(paramArray[i]) || .. ||
        // !isParamEmpty(paramArray[SIZE])
        boolean[] suffixOr = new boolean[SIZE + 1];
        suffixOr[SIZE] = false;

        for (int i = SIZE - 1; i >= 0; i--) {
            suffixOr[i] = suffixOr[i + 1] || !isParamEmpty(params[i]);
        }
        //id => "filter-10330"
        final Long parsedFilterId = parseFilterId(filterid);
        ApplicationUser user = authenticationContext.getLoggedInUser();
        SearchRequest filter = searchRequestService.getFilter(new JiraServiceContextImpl(user), parsedFilterId);
        if (filter == null) {
            throw new IllegalArgumentException("Invalid filter");
        }

        JqlClauseBuilder jqb = JqlQueryBuilder.newBuilder().where();
        boolean needAndSub = false;
        if (filter.getQuery().getWhereClause() != null) {
            jqb.addClause(filter.getQuery().getWhereClause());
            if (suffixOr[0]) {
                needAndSub = true;
                jqb.and().sub();
            }
        }

        boolean needOperator = false;
        for (int i = 0; i < SIZE; i++) {
            if (isParamEmpty(params[i])) continue;
            ClauseInformation clauseInformation = getFieldInformation(fields[i]);
            if (clauseInformation != null) {
                if (needOperator) jqb = "AND".equals(mode) ? jqb.and() : jqb.or();
                ConditionBuilder cb = jqb.field(clauseInformation.getJqlClauseNames().getPrimaryName());

                Operator operator = (!containsOperator(idInputs[i]))
                        ? guessFieldOperator(clauseInformation)
                        : (isolateOperator(idInputs[i]).equals("=")) ? Operator.EQUALS : Operator.LIKE;

                if (operator == Operator.LIKE) jqb = cb.like(params[i]);
                else if (operator == Operator.EQUALS) jqb = cb.eq(params[i]);
                needOperator = true;
            }
        }

        if (needAndSub) jqb.endsub();

        Query query = jqb.endWhere().buildQuery();
        String jqlQuery = searchService.getJqlString(query);
        LOG.debug("Calculated query: " + jqlQuery);

        UriBuilder builder = UriBuilder.fromUri(applicationProperties.getString(APKeys.JIRA_BASEURL));
        Collection<Issue> issues;

        issues = searchService.search(user, query, new PagerFilter<Issue>(2)).getIssues();

        if (issues.size() != 1) {
            builder.path("issues/").queryParam("filter", parsedFilterId).queryParam("jql", jqlQuery);
        } else {
            builder.path("browse").path(issues.iterator().next().getKey());
        }

        return builder;
    }

    private String isolateId(String input) {
        return input.substring(0, input.length() - 1);
    }

    private String isolateOperator(String input) {
        return input.substring(input.length() - 1);
    }

    private boolean containsOperator(String input) {
//        return !input.matches("[^=~]*");
        return input.charAt(input.length() - 1) == '=' || input.charAt(input.length() - 1) == '~';
    }
}
